import 'dart:async';
import 'package:flutter/material.dart';
import 'package:webapp/main.dart' as app;
import 'package:flutter_test/flutter_test.dart';

class AutomationUtility {
  FutureDelay(WidgetTester tester, seconds) async {
    //delay added for ui update
    var timerDone = false;
    Timer(Duration(seconds: seconds), () => timerDone = true);
    while (timerDone != true) {
      await tester.pump();
    }
  }

  loadApp(WidgetTester tester) async {
    //app loaded to test environment
    app.main();
    await tester.pumpAndSettle();
  }

  FindWidgetByKey(WidgetTester tester, Key key) async {
    var field = await find.byKey(key);
    print(field);
    await FutureDelay(tester, 10);
  }

  FindWidgetByText(WidgetTester tester, String text) async {
    var field = await find.text(text);
    print(field);
    await FutureDelay(tester, 5);
  }

  FindAndInputField(WidgetTester tester, Key key, data) async {
    var field = await find.byKey(key);
    print(field);
    await tester.enterText(field, data);
    await FutureDelay(tester, 3);
  }

  FindAndInputFieldByKeyFirst(WidgetTester tester, Key key, data) async {
    var field = await find.byKey(key).first;
    print(field);
    await tester.enterText(field, data);
    await FutureDelay(tester, 3);
  }

  FindAndInputFieldByKeyLast(WidgetTester tester, Key key, data) async {
    var field = await find.byKey(key).last;
    print(field);
    await tester.enterText(field, data);
    await FutureDelay(tester, 3);
  }

  TapButton(WidgetTester tester, Key key) async {
    var field = await find.byKey(key);
    print(field);
    await tester.tap(field);
    await FutureDelay(tester, 10);
  }

  CloseAlertDialog(WidgetTester tester, Key key) async {
    var error_alert = await find.byKey(key);
    print(error_alert);
    await tester.tap(error_alert);
    await FutureDelay(tester, 10);
  }

  ScrollWithDrag(WidgetTester tester, Key key, Offset offset) async {
    var scroll_widget = await find.byKey(key);
    print(scroll_widget);
    await tester.drag(scroll_widget, offset);
    await FutureDelay(tester, 10);
  }

  ScrollWithGesture(
      WidgetTester tester, Key key, Offset fromOffset, Offset toOffset) async {
    var gesture = await tester.startGesture(fromOffset);
    await gesture.moveBy(toOffset);
    await tester.pump();
  }

  ScrollByDragFrom(WidgetTester tester, Offset from, Offset to) async {
    await tester.dragFrom(from, to);
  }

  TextInputSubmit(WidgetTester tester) async {
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await AutomationUtility().FutureDelay(tester, 5);
  }
}
