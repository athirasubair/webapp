import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'automation_utility.dart';

void main() {
  group('Kriips Consumer App Test', () {
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    testWidgets("sanity test ", (WidgetTester tester) async {
      print("app loaded to test environment");
      await AutomationUtility().loadApp(tester);
      print("splash test started");
      await AutomationUtility().FindWidgetByKey(tester, Key('title'));
      // await AutomationUtility().TapButton(tester, Key('button'));
      // AutomationUtility().FutureDelay(tester, 30);
    });
  });
}
